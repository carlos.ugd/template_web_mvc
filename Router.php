<?php

namespace MVC;

class Router
{
    public $rutasGET = [];
    public $rutasPOST = [];
    public $rutasProtegidas = [];

    public function get($url, $fn, $priv = "")
    {
        $this->rutasGET[$url] = $fn;

        if ($priv === "lock") {
            if (!in_array($url, $this->rutasProtegidas)) {
                $this->rutasProtegidas[] = $url;
            }
        }
    }

    public function post($url, $fn, $priv = "")
    {
        $this->rutasPOST[$url] = $fn;

        if ($priv === "lock") {
            if (!in_array($url, $this->rutasProtegidas)) {
                $this->rutasProtegidas[] = $url;
            }
        }
    }

    public  function render($view, $datos = [])
    {
        foreach ($datos as $key => $value) {
            $$key = $value;
        }

        ob_start();
        include __DIR__ . "/views/$view.php";
        $contenido = ob_get_clean();
        include __DIR__ . "/views/layout.php";
    }

    public function comprobarRutas()
    {
        session_start();
        $auth = $_SESSION["login"] ?? false;

        $urlActual = $_SERVER["PATH_INFO"] ?? "/";
        $metodo = $_SERVER["REQUEST_METHOD"];

        if ($metodo === "GET") {
            $fn = $this->rutasGET[$urlActual] ?? NULL;
        } else {
            $fn = $this->rutasPOST[$urlActual] ?? NULL;
        }

        if (in_array($urlActual, $this->rutasProtegidas) && !$auth) {
            header("Location: /");
        }

        if ($fn) {
            call_user_func($fn, $this);
        } else {
            echo "Pagina No Encontrada";
        }
    }
}
