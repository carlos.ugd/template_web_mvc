<?php
require_once __DIR__ . "/../includes/app.php";

use MVC\Router;
use Controllers\ExampleController;

$router = new Router();

$router->get("/", [ExampleController::class, "index"]);

$router->comprobarRutas();
