<?php

namespace Controllers;

use MVC\Router;
use PHPMailer\PHPMailer\PHPMailer;

class ExampleController
{
    public static function index(Router $router)
    {
        $router->render("paginas/index", [
            "parametro" => "Parametro Inyectado"
        ]);
    }

    public static function contacto(Router $router)
    {
        $mensaje = null;
        $color = "";

        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            $respuesta = $_POST["contacto"];

            //Instancia
            $mail = new PHPMailer();
            //Configurar SMTP
            $mail->isSMTP();
            $mail->Host = "smtp.mailtrap.io";
            $mail->SMTPAuth = true;
            $mail->Username = "5dee55c2927743";
            $mail->Password = "419daad8539189";
            $mail->SMTPSecure = "tls";
            $mail->Port = 2525;

            //Configurar Contenido;
            $mail->setFrom("admin@bienesraices.com");
            $mail->addAddress("admin@bienesraices.com", "Bienesraices.com");
            $mail->Subject = "Tienes un Nuevo Mensaje";

            //Habilitar HTML
            $mail->isHTML(true);
            $mail->CharSet = "UTF-8";

            //Definir Contenido
            $contenido = "<html>";
            $contenido .= "<p>Tienes un nuevo mensaje</p>";
            $contenido .= "<p>Nombre: " . $respuesta["nombre"] . "</p>";

            if ($respuesta["contacto"] === "telefono") {
                $contenido .= "<p>Quiere ser contactado por telefono:</p>";
                $contenido .= "<p>Telefono: " . $respuesta["telefono"] . "</p>";
                $contenido .= "<p>Fecha Contacto: " . $respuesta["fecha"] . "</p>";
                $contenido .= "<p>Hora: " . $respuesta["hora"] . "</p>";
            } else {
                $contenido .= "<p>Quiere ser contactado por correo:</p>";
                $contenido .= "<p>Email: " . $respuesta["email"] . "</p>";
            }

            $contenido .= "<p>Mensaje: " . $respuesta["mensaje"] . "</p>";
            $contenido .= "<p>Vende o Compra: " . $respuesta["tipo"] . "</p>";
            $contenido .= "<p>Precio o Presupuesto: $" . $respuesta["precio"] . "</p>";
            $contenido .= "</html>";

            $mail->Body = $contenido;
            $mail->AltBody = "Esto es texto alternativo sin HTML";

            if ($mail->send()) {
                $mensaje = "Mensaje Enviado";
                $color = "exito";
            } else {
                $mensaje = "El Mensaje No Se Pudo Enviar";
                $color = "error";
            }
        }

        $router->render("paginas/contacto", [
            "mensaje" => $mensaje,
            "color" => $color
        ]);
    }
}
