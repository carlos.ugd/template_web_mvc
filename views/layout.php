<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Template MVC</title>
    <!-- La referencia es desde la carpeta public, el archivo se carga ahi -->
    <link rel="stylesheet" href="/build/css/app.css" />
</head>

<body>
    <!-- Operador Ternario, si $inicio tiene valor imprime "inicio" sino "" -->
    <header class="header <?php echo $inicio ? "inicio" : "" ?>">
        <div class="contenedor contenido-header">
            <div class="barra">
                <a href="/"><img src="/build/img/logo.svg" alt="Logotipo de Bienes Raices" /></a>

                <div class="mobile-menu">
                    <img src="/build/img/barras.svg" alt="icono menu responsive" />
                </div>

                <div class="derecha">
                    <img class="dark-mode-boton" src="/build/img/dark-mode.svg" alt="boton dark mode" />
                    <nav data-cy="navegacion-header" class="navegacion">
                        <a href="/">Nosotros</a>
                        <a href="/">Propiedades</a>
                        <a href="/">Blog</a>
                        <a href="/">Contacto</a>
                    </nav>
                </div>
            </div>

            <h1 data-cy="heading-sitio">Venta de Casas y Departamentos Exclusivos de Lujo</h1>
        </div>
    </header>

    <!-- Inyecta la vista de la funcion render() de la clase Router -->
    <?php echo $contenido; ?>

    <footer class="footer seccion">
        <div class="contenedor contenedor-footer">
            <nav data-cy="navegacion-footer" class="navegacion">
                <a href="/">Nosotros</a>
                <a href="/">Propiedades</a>
                <a href="/">Blog</a>
                <a href="/">Contacto</a>
            </nav>
        </div>

        <p class="copyright">Todos los Derechos Reservados <?php echo date("Y") ?> &copy;</p>
    </footer>
    <!-- La referencia es desde la carpeta public, el archivo se carga ahi -->
    <script src="/build/js/bundle.min.js"></script>
</body>

</html>