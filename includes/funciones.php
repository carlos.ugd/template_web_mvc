<?php

define("TEMPLATES_URL", __DIR__ . "/templates");
define("FUNCIONES_URL", __DIR__ . "funciones.php");
define("CARPETA_IMAGENES", $_SERVER["DOCUMENT_ROOT"] . "/imagenes/");

function incluirTemplate(string $nombre, bool $inicio = false)
{
    include TEMPLATES_URL . "/${nombre}.php";
}

function estaAutenticado()
{
    session_start();
    if (!$_SESSION["login"]) {
        header("location: /");
    }
}

function debug($var)
{
    echo "<pre>";
    var_dump($var);
    echo "</pre>";
    exit;
}

function sanitizar($html): string
{
    $s = htmlspecialchars($html);
    return $s;
}

function validarTipoContenido($tipo)
{
    $tipos = ["vendedor", "propiedad"];
    return in_array($tipo, $tipos);
}

function mostrarNotificacion($codigo)
{
    $mensajes = [
        1 => "Creado Correctamente",
        2 => "Actualizado Correctamente",
        3 => "Eliminado Correctamente"
    ];

    if (!array_key_exists($codigo, $mensajes)) {
        return $mensaje = false;
    }

    return $mensaje = $mensajes[$codigo];
}


function validarORedireccionar($url)
{
    $id = $_GET["id"];
    $id = filter_var($id, FILTER_VALIDATE_INT);

    if (!$id) {
        header("location: ${url}");
    }

    return $id;
}
