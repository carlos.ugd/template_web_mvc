<?php

namespace Model;

class ActiveRecord
{
    protected static $db;
    protected static $columnasDB = [];
    protected static $tabla = '';

    protected static $errores = [];

    public static function setDB($database)
    {
        self::$db = $database;
    }

    public function guardar()
    {
        if (!is_null($this->id)) {
            $this->actualizar();
        } else {
            $this->crear();
        }
    }

    public function crear()
    {
        $atributos = $this->sanitizaratributos();

        //JOIN crea un string a partir de un arreglo
        $llaves = join(", ", array_keys($atributos));
        $valores = join("', '", array_values($atributos));

        $query = "INSERT INTO " . static::$tabla . " (${llaves}) VALUES ('${valores}')";

        $resultado = self::$db->query($query);

        if ($resultado) {
            header("Location:/admin?msg=1");
        }
    }

    public function actualizar()
    {
        $atributos = $this->sanitizaratributos();
        $valores = [];
        foreach ($atributos as $key => $value) {
            $valores[] = "{$key} = '{$value}'";
        }

        $query = "UPDATE " . static::$tabla . " SET " . join(", ", $valores) . " WHERE id = '" . self::$db->escape_string($this->id) . "' LIMIT 1";

        $resultado = self::$db->query($query);

        if ($resultado) {
            header("Location:/admin?msg=2");
        }
    }

    public function eliminar()
    {
        $query = "DELETE FROM " . static::$tabla . " WHERE id = '" . self::$db->escape_string($this->id) . "' LIMIT 1";
        $resultado = self::$db->query($query);
        if ($resultado) {
            $this->borrarImagen();
            header('location:/admin?msg=3');
        }
    }

    public function atributos()
    {
        $atributos = [];
        foreach (static::$columnasDB as $columna) {
            if ($columna === "id") {
                continue;
                //Si encuentra el id ejecuta el continue para saltar a la siguiente iteracion
            }
            $atributos[$columna] = $this->$columna;
        }
        return $atributos;
    }

    public function sanitizaratributos()
    {
        $atributos = $this->atributos();
        $sanitizado = [];

        foreach ($atributos as $key => $value) {
            $sanitizado[$key] = self::$db->escape_string($value);
        }

        return $sanitizado;
    }

    public function setImagen($imagen)
    {
        if (!is_null($this->id)) {
            $this->borrarImagen();
        }
        if ($imagen) {
            $this->imagen = $imagen;
        }
    }

    public function borrarImagen()
    {
        $existe = file_exists(CARPETA_IMAGENES . $this->imagen);
        if ($existe) {
            unlink(CARPETA_IMAGENES . $this->imagen);
        }
    }

    public static function getErrores()
    {
        return static::$errores;
    }

    public function validar()
    {
        static::$errores = [];
        return static::$errores;
    }

    public static function all()
    {
        $query = "SELECT * FROM " . static::$tabla;
        return $resultado = self::consultarSQL($query);
    }

    public static function get($cantidad)
    {
        $query = "SELECT * FROM " . static::$tabla . " LIMIT " . $cantidad;
        return $resultado = self::consultarSQL($query);
    }

    public static function consultarSQL($query)
    {
        $resultado = self::$db->query($query);
        $array = [];

        while ($registro = $resultado->fetch_assoc()) {
            $array[] = static::crearObjeto($registro);
        }
        $resultado->free();

        return $array;
    }

    private static function crearObjeto($registro)
    {
        $objeto = new static; //Hace referencia al objeto que lo esta heredeando

        foreach ($registro as $key => $value) {
            if (property_exists($objeto, $key)) {
                $objeto->$key = $value;
            }
        }
        return $objeto;
    }

    public static function find($id)
    {
        $query = "SELECT * FROM " . static::$tabla . " WHERE id = ${id}";
        $resultado = self::consultarSQL($query);
        return array_shift($resultado); //Devuelve la primer posicion un arraay
    }

    public function sincronizar($args = [])
    {
        foreach ($args as $key => $value) {
            if (property_exists($this, $key) && !is_null($value)) {
                $this->$key = $value;
            }
        }
    }
}
